<?php

namespace App\Controller\Api\V1;

use App\Utils\Api;
use App\Utils\ApiException;
use App\Utils\RequestTool;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecipeController extends AbstractController
{
    use RequestTool;

    private $url = "https://api.punkapi.com/v2/beers";

    /**
     * @Route("/api/v1/recipe", name="api_recipe_v1", methods={"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(Request $request)
    {

        $data_beers = $this->getDataBeer($request);

        if (!empty($data_beers['http_code'])) {
            return new JsonResponse(['message' => $data_beers['message']], $data_beers['http_code']);
        }

        $keys = [
            "id",
            "name",
            "description"
        ];

        $data_to_send = $this->getBeer($data_beers, $request->query->get('search'), $keys);

        return new JsonResponse(['message' => 'Your request was successfully submitted', 'data' => $data_to_send], 200);
    }

    /**
     * @Route("/api/v1/recipe/view_data", name="api_v1_dataview_recipe", methods={"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function viewData(Request $request)
    {

        $data_beers = $this->getDataBeer($request);

        if (!empty($data_beers['http_code'])) {
            return new JsonResponse(['message' => $data_beers['message']], $data_beers['http_code']);
        }

        $keys = [
            "id",
            "name",
            "description",
            "image_url",
            "tagline",
            "first_brewed"
        ];

        $data_to_send = $this->getBeer($data_beers, $request->query->get('search'), $keys);

        return new JsonResponse(['message' => 'Your request was successfully submitted', 'data' => $data_to_send], 200);
    }

    /**
     * Get data from url
     * @param $request Object that has the request data
     * @return array|mixed Return data from request or an error if something is wrong with the request
     */
    public function getDataBeer($request) {
        try {
            $api = new Api($request);
        } catch (ApiException $e){
            return ['http_code' => 400, 'message' => 'Invalid Token'];
        }

        if (!$request->query->has('search')) {
            return ['http_code' => 400, 'message' => 'Missing search parameter'];
        }

        $data_beers = $this->makeRequest($this->url);
        if (!empty($data["http_code"])) {
            return ['http_code' => $data["http_code"], 'message' => $data["error_message"]];
        } else if (empty($data_beers)) {
            return ['http_code' => 400, 'message' => 'There is no data'];
        }

        return $data_beers;
    }

    /**
     * Function to get beers depending on the $search parameter
     * @param array $data_beers Data from the request to the Api
     * @param string $search Variable to find the beers
     * @param array $keys Variable to find in the $data_beers
     * @return array Data to return from request
     */
    public function getBeer($data_beers, $search, $keys) {
        $data_to_send = [];

        foreach ($data_beers as $data) {
            foreach ($data["food_pairing"] as $food) {
                if ($this->strposArray($food, $search)) {
                    $array_data = [];
                    foreach ($keys as $key) {
                        $array_data[$key] = $data[$key];
                    }
                    $data_to_send[] = $array_data;
                    continue 2;
                }
            }
        }

        return $data_to_send;
    }

    /**
     * Function to find a needle, can be a string or an array
     * @param $haystack It is the data to search
     * @param $needle It is the string find
     * @param int $offset
     * @return bool
     */
    function strposArray($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) {
                return true;
            }
        }
        return false;
    }
}
