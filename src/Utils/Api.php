<?php


namespace App\Utils;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class Api
{

    /**
     * Api constructor.
     * @param Request $request
     * @throws ApiException
     */
    public function __construct(Request $request)
    {

        if($request->headers->get('token') === null){
            throw new ApiException('Missing token');
        }

        if($request->headers->get('token') !== 'JnomXfjn1tFeYJrHShb0'){
            throw new ApiException('Invalid token');
        }
    }

}