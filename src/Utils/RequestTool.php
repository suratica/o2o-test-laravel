<?php

namespace App\Utils;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

trait RequestTool {

    /**
     * @param string $url Parameter in order to make the request
     * @param null $headers
     * @param string $type_request
     * @return mixed data from request
     *
     */
    public function makeRequest($url, $headers = null, $type_request = "GET") {
        $client = new Client();

        try {
            if (empty($headers)) {
                $res = $client->request($type_request, $url);
            } else {
                $res = $client->request($type_request, $url, [
                    'headers' => $headers
                ]);
            }

        } catch (TransferException $e) {
            $array_data["http_code"] = 400;
            $array_data["message"] = "There is an error connecting to the Api";
            return $array_data;
        }
        $status_code = $res->getStatusCode();

        if ($status_code == 200) {
            $array_data = json_decode($res->getBody(), true);
        } else {
            $array_data["http_code"] = $status_code;
            $array_data["error_message"] = "There is an error connecting to the Api";
        }

        return $array_data;
    }


}