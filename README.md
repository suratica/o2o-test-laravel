# o2o-test-laravel

This application is an exercise from a [technical interview](https://github.com/mo2o/backend-exercise).

## Installation

First clone this repo to your local machine.

Use the package manager composer to install the dependencies required for this project

```bash
$ composer install
```

## Documentation

There are two services to get data from [PunkApi](https://punkapi.com/documentation/v2).

- The first feature return id, name and description filtered by a string.
- The second feature return id, name, description, image, tagline and first_brewed filtered by a string.

These two feature have two different endpoints. The endpoints to retrieve the data are:
- First exercise: [domain_name/api/v1/recipe](domain_name/api/v1/recipe)
- Second exercise: [domain_name/api/v1/recipe/view_data](domain_name/api/v1/recipe/view_data)

The two endpoints are HTTP GET request. In order to make the request, it is needed a token header which value is "JnomXfjn1tFeYJrHShb0" and "search" variable as a parameter.

| Endpoint     | Parameters    | Headers |
| --------|---------|-------|
| [domain_name/api/v1/recipe](domain_name/api/v1/recipe)  | search: string   | token: JnomXfjn1tFeYJrHShb0    |
| [domain_name/api/v1/recipe/view_data](domain_name/api/v1/recipe/view_data) | search: string |token: JnomXfjn1tFeYJrHShb0    |

## Dependencies


The dependencies are:
- guzzlehttp/guzzle
- Doctrine
- FOSRESTBundle
- symfony/serializer-pack
- cors
- http-foundation
- maker-bundle
- monolog

## License
[MIT](https://choosealicense.com/licenses/mit/)
