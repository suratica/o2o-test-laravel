<?php

namespace App\Tests\Controller\Api\V1;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecipeControllerTest extends WebTestCase
{

    public function testIndexWithoutToken() {
        $client = new Client();

        $url = "http://192.168.10.10/api/v1/recipe";
        $headers = null;
        try {
            $res = $client->request("GET", $url, [
                'headers' => $headers
            ]);
        } catch (TransferException $e) {
            $status_code = $e->getResponse()->getStatusCode();
        }

        $this->assertEquals(400, $status_code);
    }

    public function testIndexWithEmptyParameter() {
        $client = new Client();

        $url = "http://192.168.10.10/api/v1/recipe";

        $headers = ["token" => "JnomXfjn1tFeYJrHShb0"];
        try {
            $res = $client->request("GET", $url, [
                'headers' => $headers
            ]);
        } catch (TransferException $e) {
            $status_code = $e->getResponse()->getStatusCode();
        }

        $this->assertEquals(400, $status_code);
    }

    public function testIndex() {
        $client = new Client();

        $url = "http://192.168.10.10/api/v1/recipe?search=garlic";

        $headers = ["token" => "JnomXfjn1tFeYJrHShb0"];
        try {
            $res = $client->request("GET", $url, [
                'headers' => $headers
            ]);
        } catch (TransferException $e) {
            $status_code = $e->getResponse()->getStatusCode();
        }

        if (empty($status_code)) {
            $status_code = $res->getStatusCode();
            $data_values = json_decode($res->getBody(), true);
        }

        $this->assertEquals(200, $status_code);

        foreach ($data_values['data'] as $data) {
            $this->assertArrayHasKey('id', $data);    // Succeeds
            $this->assertArrayHasKey('name', $data);
            $this->assertArrayHasKey('description', $data);
        }
    }

    public function testViewDataWithoutToken() {
        $client = new Client();

        $url = "http://192.168.10.10/api/v1/recipe/view_data";
        $headers = null;
        try {
            $res = $client->request("GET", $url, [
                'headers' => $headers
            ]);
        } catch (TransferException $e) {
            $status_code = $e->getResponse()->getStatusCode();
        }

        $this->assertEquals(400, $status_code);
    }

    public function testViewDataWithEmptyParameter() {
        $client = new Client();

        $url = "http://192.168.10.10/api/v1/recipe/view_data";

        $headers = ["token" => "JnomXfjn1tFeYJrHShb0"];
        try {
            $res = $client->request("GET", $url, [
                'headers' => $headers
            ]);
        } catch (TransferException $e) {
            $status_code = $e->getResponse()->getStatusCode();
        }

        $this->assertEquals(400, $status_code);
    }

    public function testViewData() {
        $client = new Client();

        $url = "http://192.168.10.10/api/v1/recipe/view_data?search=garlic";

        $headers = ["token" => "JnomXfjn1tFeYJrHShb0"];
        try {
            $res = $client->request("GET", $url, [
                'headers' => $headers
            ]);
        } catch (TransferException $e) {
            $status_code = $e->getResponse()->getStatusCode();
        }

        if (empty($status_code)) {
            $status_code = $res->getStatusCode();
            $data_values = json_decode($res->getBody(), true);
        }

        $this->assertEquals(200, $status_code);

        foreach ($data_values['data'] as $data) {
            $this->assertArrayHasKey('id', $data);    // Succeeds
            $this->assertArrayHasKey('name', $data);
            $this->assertArrayHasKey('description', $data);
            $this->assertArrayHasKey('image_url', $data);
            $this->assertArrayHasKey('tagline', $data);
            $this->assertArrayHasKey('first_brewed', $data);
        }
    }


}